# Bootloader Application
The following is a list of notes regarding the bootloader in this directory.

## General Requirements
You'll need the following tools:

 - avrdude
 - srecord
 - avr-gcc

## Compiling
    $ make

## Uploading the bootloader

This will only flash the bootloader, You need to have a valid recovery image stored in flash

    $ make upload

## Prepend an application to the bootloader

This will flash the bootloader, the application an program the avr fuses

    $ make full PGM=../../demo/demo-http/rest-server-example.avr-homadeus
