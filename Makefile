PROGRAM    = main

F_CPU = 16000000L
TARGET = atmega128rfa1
LDSECTION  = --section-start=.text=0x1F000,--relax
# LDSECTION  = --section-start=.text=0x00000,--relax

classpathify = $(subst $(eval) ,:,$1)
APPS       = spi_driver spi_flash tiki_utils bootloader_control
APPDIRS    = $(addprefix ../,$(APPS))
VPATH      = $(call classpathify,$(APPDIRS))

OBJ        = $(PROGRAM).o spi_driver.o spi_flash.o bootloader_control.o
OPTIMIZE   = -Os

# Override is only needed by avr-lib build system.
override CFLAGS        = -std=gnu99 -g -Wextra -Wall $(OPTIMIZE) -mmcu=$(TARGET) -DF_CPU=$(F_CPU) $(foreach app,$(APPDIRS),-I$(app)) -ffunction-sections -Werror -Wno-unused-function \
	-Wno-missing-field-initializers \
	-fverbose-asm \
	-save-temps=obj \
	-fno-inline-small-functions \
	-ffreestanding \
	-funsigned-char \
	-funsigned-bitfields \
	-fpack-struct \
	-fshort-enums \
	-finline-limit=3 \
	-fno-inline-small-functions \
	-ffunction-sections \
	-fdata-sections \
	-mcall-prologues \
	-fno-tree-scev-cprop \
	-fno-split-wide-types \

override LDFLAGS       = -Wl,$(LDSECTION) -Wl,-gc-sections \
	-fwhole-program

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump
CC             = avr-gcc

AVRDUDE        = avrdude -p atmega128rfa1 -c usbasp

all: init clean bootloader

init:
	@test -d .obj || mkdir .obj

bootloader: $(PROGRAM).hex $(PROGRAM).elf

dragon: bootloader
	sudo avrdude -e -B 10 -c dragon_isp -F -p atmega128rfa1 -P usb -V -D -U flash:w:$(PROGRAM).hex:i 2>&1

test:
	sudo avrdude -c arduino -b 57600 -p atmega128rfa1 -P ~/vttyUSB0 -V -D -U flash:w:firmware.test:i 2>&1 || true

testrec:
	sudo avrdude -e -c arduino -b 57600 -p atmega128rfa1 -P ~/vttyUSB0 -V -D -U flash:w:firmware.test:i 2>&1 || true

fuse:
	$(AVRDUDE)  -U lfuse:w:0xff:m -U hfuse:w:0xd0:m -U efuse:w:0xf5:m

upload: bootloader
	$(AVRDUDE)  -U flash:w:$(PROGRAM).hex

full: bootloader fuse
	$(OBJCOPY) -j .text -j .data -O ihex $(PGM) pgm.hex
	srec_cat pgm.hex -intel $(PROGRAM).hex -intel -o full.hex -intel
	$(AVRDUDE)  -U flash:w:full.hex

%.elf: $(addprefix .obj/,$(OBJ))
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^
	avr-size $@

clean:
	rm -rf *.o *.elf *.lst *.map *.sym *.lss *.eep *.srec *.bin *.hex

.obj/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@
