#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/boot.h>
#include <avr/io.h>
#include <util/crc16.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include <spi/flash.h>
#include <homadeus/utils/utils.h>
#include <homadeus/bootloader/bootloader.h>

#define BAUD 57600
#include <util/setbaud.h>

#define loop() do { DEBUG("Looping"); while(1) { wdt_disable(); } } while(0)

#define ERROR(x) \
    serial_write_bytes(sizeof(LINE_STRING ":" x "\r\n"), (uint8_t *) (LINE_STRING ":" x "\r\n"))

#define HAVE_DEBUG
#if defined(HAVE_DEBUG)
#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) #x
#define LINE_STRING STRINGIZE(__LINE__)

#define DEBUG(x) \
    ERROR(x)

#define DEBUGR(x) \
    serial_write_bytes(sizeof(x "\r\n"), (uint8_t *) (x "\r\n"))

#define DEBUGNB(s, n, x) \
    do { \
        serial_write_bytes(sizeof(s), (void*) s); \
        for (int i = 0; i < n; i++) { \
            uint8_t b = *(((uint8_t*)x) + n - 1 - i); \
            serial_write_byte(p[b >> 4]); \
            serial_write_byte(p[b & 0x0F]); \
        } \
        DEBUGR("");\
    } while(0)
#else

#define DEBUG(...) do { /**/ } while(0)
#define DEBUGR(...) do { /**/ } while(0)
#define DEBUGNB(...) do { /**/ } while(0)

#endif

struct spi_slave slave_ = { 0 }, *slave = NULL;
struct spi_flash flash_ = { 0 }, *flash = NULL;
struct bootloader_status status = { 0 };

static void serial_write_byte(uint8_t byte);
static uint8_t serial_write_bytes(uint16_t count, const uint8_t* buf);

const char p[] = "0123456789abcdef";
static void debug_addr(uint32_t address) {
    uint32_t base =  address;
    for(uint8_t i = 0; address && base && i < sizeof(base); i++) {
        uint8_t b = *(((uint8_t*)&base) + 3 - i);
        serial_write_byte(p[b >> 4]);
        serial_write_byte(p[b & 0x0F]);
    }
}

void snip_buff(void *buffer, int32_t len);
void snip_buff(void *buffer, int32_t len) {
    for(int32_t i = 0; i < len; i++) {
        uint8_t buf = *((uint8_t *) buffer + i);
        serial_write_byte(p[(buf >> 4) & 0x0F]);
        serial_write_byte(p[buf & 0x0F]);

        if ((i+1) % 8 == 0)
            serial_write_bytes(2, (uint8_t*)"\r\n");
    }
    DEBUG("\r\n");
}

void (*app)(void) = 0x0000;

static void start_app(void) __attribute__((naked));
static void start_app(void) {
#ifdef USE_2X
    UCSR0A &= ~(1 << U2X0);
#endif
    cli();
    boot_rww_enable_safe();
    app();
}

uint8_t buffer[SPM_PAGESIZE];

static int bootloader_firmware_read(struct spi_flash *flash, bootloader_image *image, enum bootloader_fw firmware) {

    uint32_t base = flash->size;
    uint16_t fw_crc16 = CRC16_INIT;

    switch (firmware) {
        case USER_FIRMWARE:
            base = FIRMWARE_BASE_ADDRESS(flash);
            break;
        default:
            base = RECOVERY_BASE_ADDRESS(flash);
            break;
    }

    int ret = spi_flash_read(flash, base, sizeof(*image), image);
    if (ret) {
        goto err_image;
    }
    image->fw_offset = PROGRAM_OFFSET(flash, base);

    if (image->fw_length > PROGRAM_SIZE) {
        ERROR("Wrong image size");
        goto err_image;
    }
    if (image->crc16_read != image->crc16_saved) {
        ERROR("Wrong CRC16");
        goto err_image;
    }

    for (uint32_t len = 0; len < image->fw_length; len += BOOTLOADER_FLASH_BUFFER) {

        int remaining = min(sizeof(buffer), image->fw_length - len);

        ret = spi_flash_read(flash, image->fw_offset + len, remaining, buffer);
        if (ret) {
            goto err_image;
        }
        fw_crc16 = crc16(fw_crc16, buffer, remaining);
    }

    image->crc16_read = fw_crc16;

    return image->crc16_saved != image->crc16_read;

err_image:
    return 1;
}

static int bootloader_open_flash(struct spi_slave *slave, struct spi_flash *flash) {
    slave->cs_port_dir = &DDRB;
    slave->cs_port     = &PORTB;
    slave->cs_pin      = PB0;

    return spi_flash_probe(flash, slave);
}

static void bootloader_erase_page(uint32_t page) {
    uint8_t sreg = SREG;
    cli();

    boot_page_erase_safe(page);

    SREG = sreg;
}

static int bootloader_write_page(uint32_t page, uint8_t *buffer) {

    uint8_t sreg = SREG;
    cli();

    bootloader_erase_page(page);

    for (uint16_t i = 0; i < SPM_PAGESIZE; i += 2, buffer += 2) {
        uint16_t *word = (uint16_t *) buffer;
        boot_page_fill_safe(page + i, *word);
    }

    boot_page_write_safe(page);

    SREG = sreg;

    return 0;
}


static int bootloader_firmware_write(enum bootloader_fw firmware, uint8_t retry) {
    int ret = 1;

    bootloader_image image = { 0 };
    image.fw_type = INVALID_FIRMWARE;

    DEBUG("bootloader_firmware_write");

    switch (firmware) {
        case USER_FIRMWARE:
            DEBUG("USER_FIRMWARE");
            if (bootloader_firmware_read(flash, &image, firmware)) {
                firmware = FAILSAFE_FIRMWARE;
                ERROR("User firmware read failed");
            } else {
                break;
            }

        case FAILSAFE_FIRMWARE:
            DEBUG("FAILSAFE_FIRMWARE");
            if (bootloader_firmware_read(flash, &image, firmware)) {
                ERROR("Failsafe firmware read failed");
                firmware = INVALID_FIRMWARE;
            } else {
                break;
            }

        default:
            firmware = INVALID_FIRMWARE;
    }

    if (firmware == INVALID_FIRMWARE) {
        goto err_read_only;
    }

    bootloader_erase_page(0);

    /* Flash the last page last to prevent corruption */
    for(uint32_t len = BOOTLOADER_FLASH_BUFFER; len < image.fw_length; len += BOOTLOADER_FLASH_BUFFER) {
        int remaining = min(sizeof(buffer), image.fw_length - len);

        memset(buffer, 0xFF, sizeof(buffer));

        ret = spi_flash_read(flash, image.fw_offset + len, remaining, buffer);
        if (ret) {
            goto err_flash;
        }
        bootloader_write_page(len, buffer);
    }

    ret = spi_flash_read(flash, image.fw_offset, BOOTLOADER_FLASH_BUFFER, buffer);

    bootloader_write_page(0, buffer);

    return 0;

err_read_only:
    return 1;

err_flash:
    if (retry) {
        // We can try again at least;
        return bootloader_firmware_write(firmware, 0);
    } else if (firmware == USER_FIRMWARE) {
        // We failed, we're going to try another fw
        return bootloader_firmware_write(FAILSAFE_FIRMWARE, 1);
    }
    return ret;
}

static int bootloader_app_valid() {
    return pgm_read_word(0) != 0xFFFF;
}

void serial_init() {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
#if USE_2X
    UCSR0A |= (1 << U2X0);
#else
    UCSR0A &= ~(1 << U2X0);
#endif
    UCSR0B = _BV(TXEN0)|_BV(RXEN0);
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
}

static uint8_t serial_read() {
    loop_until_bit_is_set(UCSR0A, RXC0);
    return UDR0;
}

static void serial_read_bytes(uint16_t count, uint8_t *buf) {
    uint8_t *ptr = buf;
    for (uint16_t i = 0; i < count; i++) {
        uint8_t val = serial_read();
        if (buf != NULL) {
            *ptr++ = val;
        }
    }
}

static uint8_t serial_write_bytes(uint16_t count, const uint8_t* buf) {
    for (uint16_t i = 0; i < count; i++) {
        loop_until_bit_is_set(UCSR0A, UDRE0);
        UDR0 = *buf++;
    }

    return 0;
}

static void serial_write_byte(uint8_t byte) {
    serial_write_bytes(1, &byte);
}


uint8_t mcusr_mirror __attribute__ ((section (".noinit")));
void get_mcusr(void) {
    mcusr_mirror = MCUSR;
    MCUSR &= ~_BV(WDRF);
    wdt_disable();
    if (mcusr_mirror & _BV(WDRF)) {
        MCUSR |= _BV(WDRF);
    }
}

void flash_init() {
    int ret = bootloader_open_flash(&slave_, &flash_);
    if (!ret) {
        slave = &slave_;
        flash = &flash_;
        DEBUG("OK");
    } else {
        ERROR("Unable to open flash chip");
        loop();
    }
}

void main() {
    get_mcusr();
    serial_init();

    bootloader_status_read(&status);

    if (mcusr_mirror & _BV(WDRF)) {
        ERROR("[!] Boot from watchdog reset");
        status.watchdog_reset_nr++;
    } else if (mcusr_mirror & _BV(PORF) || mcusr_mirror & _BV(EXTRF)) {
        ERROR("[!] Boot from user reset");
        status.user_reset_nr++;
    }

    if (status.watchdog_reset_nr > 4 || status.user_reset_nr > 4) {
        ERROR("[!] Failsafe firmware should be written");
        status.valid = 1;
        status.dump_firmware = 0;
        status.write_failsafe_pgm = 1;
    }

    if (status.valid) {
        flash_init();
        if (status.write_failsafe_pgm || !bootloader_app_valid() ) {
            DEBUG("Writing FAILSAFE_FIRMWARE");
            if(bootloader_firmware_write(FAILSAFE_FIRMWARE, 1)) {
                ERROR("Error writing FAILSAFE_FIRMWARE");
            }
            status.user_reset_nr = 0;
            status.watchdog_reset_nr = 0;
            bootloader_status_clear(&status);
        } else if (status.write_user_pgm) {
            DEBUG("Writing USER_FIRMWARE");
            if(bootloader_firmware_write(USER_FIRMWARE, 1)) {
                ERROR("Error writing FAILSAFE_FIRMWARE");
            }
            bootloader_status_clear(&status);
            status.user_reset_nr = 0;
            status.watchdog_reset_nr = 0;
            bootloader_status_clear(&status);
        }
    }

    bootloader_status_write(&status);
    DEBUG("Booting program\r\n");
    wdt_enable(WDTO_8S);
    start_app();

}
